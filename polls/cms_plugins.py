# -*- coding: utf-8 -*-

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from polls.models import PollPlugin as PollPluginModel
from django.utils.translation import ugettext as _


class PollPlugin(CMSPluginBase):
    model = PollPluginModel # model, który przechowuje dane o wtyczce
    name = _("Poll Plugin") # nazwa wtyczki
    render_template = "polls/plugin.html" # szablon do renderowania pluginu

    def render(self, context, instance, placeholder):
        context.update({'instance':instance})
        return context

plugin_pool.register_plugin(PollPlugin) # rejestracja wtyczki
