from django.contrib import admin
from models import Poll, Choice

# admin.site.register(Poll)
# admin.site.register(Choice)


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3
    ordering = ('choice', )


class PollAdmin(admin.ModelAdmin):
    list_display = ('pub_date', 'question', 'published_today')
    list_filter = ('pub_date', )
    ordering = ('pub_date', )
    inlines = (ChoiceInline, )
    search_fields = ('question',)

admin.site.register(Poll, PollAdmin)